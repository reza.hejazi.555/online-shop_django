from django.db import models

class promotion(models.Model):
    description = models.CharField(max_length=255)
    discount = models.FloatField()

class collection(models.Model):
    title = models.CharField(max_length=255)

class product(models.Model):
    title = models.CharField(max_length=255,)
    slug = models.SlugField()
    description = models.TextField()
    unit_price = models.DecimalField(max_digits=6,decimal_places=2)
    inventory = models.IntegerField()
    last_update = models.DateTimeField(auto_now_add=True)
    collection = models.ForeignKey(collection, on_delete=models.PROTECT)
    promotions = models.ManyToManyField(promotion)

class customer(models.Model):
    membership_Bronze = 'B'
    membership_Silver = 'S'
    membership_Gold = 'G'
    membership_CHOICES = [
        (membership_Bronze, 'Bronze'),
        (membership_Silver, 'Silver'),
        (membership_Gold, 'Gold'),
    ]
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(unique=True)
    phone = models.CharField(max_length=255)
    birth_date = models.DateField(null=True)
    membership = models.CharField(max_length=1 , choices=membership_CHOICES , default=membership_Bronze)
    class Meta:
        db_table = 'store_customers'
        indexes = [
        models.Index(fields=['last_name' , 'first_name'])
    ]


class Order(models.Model):
    PAYMENT_STATUS_PENDING = 'P'
    PAYMENT_STATUS_COMPLETE = 'C'
    PAYMENT_STATUS_FAILED = 'F'
    payment_status_CHOICES = [
        (PAYMENT_STATUS_PENDING , 'pending') ,
        (PAYMENT_STATUS_COMPLETE , 'complete') ,
        (PAYMENT_STATUS_FAILED , 'Failed') 
    ]
    placed_at = models.DateTimeField( auto_now_add=True)
    payment_status = models.CharField(
        max_length=1 , choices=payment_status_CHOICES , default=PAYMENT_STATUS_PENDING)
    customer = models.ForeignKey(customer, on_delete=models.PROTECT)

class OrderItem(models.Model):
    Order = models.ForeignKey(Order, on_delete=models.PROTECT)
    product = models.ForeignKey(product, on_delete=models.PROTECT)
    quantity = models.PositiveSmallIntegerField()
    unit_price = models.DecimalField(max_digits=6 , decimal_places=2)


class Address(models.Model):
    street = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    customer = models.ForeignKey(
        customer, on_delete=models.CASCADE)
    zip = models.CharField(max_length=15 , default=123)

class cart (models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

class cartItem(models.Model):
    cart = models.ForeignKey(cart, on_delete=models.CASCADE)
    product = models.ForeignKey(product , on_delete=models.CASCADE)
    quantity = models.PositiveSmallIntegerField()

